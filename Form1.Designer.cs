namespace Frageboogen
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtn_female = new System.Windows.Forms.RadioButton();
            this.rbtn_male = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.check_C_sharp = new System.Windows.Forms.CheckBox();
            this.check_C_plus_plus = new System.Windows.Forms.CheckBox();
            this.check_C = new System.Windows.Forms.CheckBox();
            this.lbl_output = new System.Windows.Forms.Label();
            this.cmd_output = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtn_female);
            this.groupBox1.Controls.Add(this.rbtn_male);
            this.groupBox1.Location = new System.Drawing.Point(49, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 134);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Geschlecht";
            // 
            // rbtn_female
            // 
            this.rbtn_female.AutoSize = true;
            this.rbtn_female.Location = new System.Drawing.Point(28, 80);
            this.rbtn_female.Name = "rbtn_female";
            this.rbtn_female.Size = new System.Drawing.Size(53, 20);
            this.rbtn_female.TabIndex = 1;
            this.rbtn_female.TabStop = true;
            this.rbtn_female.Text = "Frau";
            this.rbtn_female.UseVisualStyleBackColor = true;
            // 
            // rbtn_male
            // 
            this.rbtn_male.AutoSize = true;
            this.rbtn_male.Location = new System.Drawing.Point(28, 37);
            this.rbtn_male.Name = "rbtn_male";
            this.rbtn_male.Size = new System.Drawing.Size(59, 20);
            this.rbtn_male.TabIndex = 0;
            this.rbtn_male.TabStop = true;
            this.rbtn_male.Text = "Mann";
            this.rbtn_male.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.check_C_sharp);
            this.groupBox2.Controls.Add(this.check_C_plus_plus);
            this.groupBox2.Controls.Add(this.check_C);
            this.groupBox2.Location = new System.Drawing.Point(325, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(157, 196);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kenntnisse";
            // 
            // check_C_sharp
            // 
            this.check_C_sharp.AutoSize = true;
            this.check_C_sharp.Location = new System.Drawing.Point(28, 134);
            this.check_C_sharp.Name = "check_C_sharp";
            this.check_C_sharp.Size = new System.Drawing.Size(43, 20);
            this.check_C_sharp.TabIndex = 2;
            this.check_C_sharp.Text = "C#";
            this.check_C_sharp.UseVisualStyleBackColor = true;
            // 
            // check_C_plus_plus
            // 
            this.check_C_plus_plus.AutoSize = true;
            this.check_C_plus_plus.Location = new System.Drawing.Point(28, 82);
            this.check_C_plus_plus.Name = "check_C_plus_plus";
            this.check_C_plus_plus.Size = new System.Drawing.Size(50, 20);
            this.check_C_plus_plus.TabIndex = 1;
            this.check_C_plus_plus.Text = "C++";
            this.check_C_plus_plus.UseVisualStyleBackColor = true;
            // 
            // check_C
            // 
            this.check_C.AutoSize = true;
            this.check_C.Location = new System.Drawing.Point(28, 39);
            this.check_C.Name = "check_C";
            this.check_C.Size = new System.Drawing.Size(36, 20);
            this.check_C.TabIndex = 0;
            this.check_C.Text = "C";
            this.check_C.UseVisualStyleBackColor = true;
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(49, 246);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(0, 16);
            this.lbl_output.TabIndex = 2;
            // 
            // cmd_output
            // 
            this.cmd_output.Location = new System.Drawing.Point(52, 332);
            this.cmd_output.Name = "cmd_output";
            this.cmd_output.Size = new System.Drawing.Size(75, 23);
            this.cmd_output.TabIndex = 3;
            this.cmd_output.TabStop = false;
            this.cmd_output.Text = "&Ausgabe";
            this.cmd_output.UseVisualStyleBackColor = true;
            this.cmd_output.Click += new System.EventHandler(this.cmd_output_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(202, 332);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 23);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(360, 332);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 415);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_output);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Fragebogen";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtn_female;
        private System.Windows.Forms.RadioButton rbtn_male;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox check_C_sharp;
        private System.Windows.Forms.CheckBox check_C_plus_plus;
        private System.Windows.Forms.CheckBox check_C;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.Button cmd_output;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
    }
}

