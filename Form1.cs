using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frageboogen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_output_Click(object sender, EventArgs e)
        {
            if (rbtn_male.Checked)
                lbl_output.Text = "Mann\n";
            if (rbtn_female.Checked)
                lbl_output.Text = "Frau\n";
            if (check_C.Checked)
                lbl_output.Text += "Kenntnisse in  C\n";
            if (check_C_plus_plus.Checked)
                lbl_output.Text += "Kenntnisse in C++\n";
            if (check_C_sharp.Checked)
                lbl_output.Text += "Kenntnisse in C#\n";
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            lbl_output.Text = null;
            rbtn_male.Checked = rbtn_female.Checked = check_C.Checked = check_C_plus_plus.Checked = check_C_sharp.Checked = false;
        }
    }
}
